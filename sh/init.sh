#!/usr/bin/env bash

echo "source parameters and variables"
. /home/ubuntu/repo/sh/_parameters
. /home/ubuntu/repo/sh/_variables

echo "create directory ${scale_dir}"
mkdir ${scale_dir}
mkdir ${scale_dir}/logs

echo "copy files to ${scale_dir}"
cp -R ${git_dir}/sh ${scale_dir}/
cp -R ${git_dir}/heat ${scale_dir}/

echo "copy haproxy conf"
cp ${git_dir}/conf/haproxy.cfg ${scale_dir}/haproxy.cfg.tmp

echo "copy control site"
rm -f /var/www/html/index.html
cp -r ${git_dir}/control_site/* /var/www/html/

echo "set owner on www"
chown -R www-data:www-data /var/www

echo "put rsyslog server config"
cp ${git_dir}/conf/49-haproxy.conf /etc/rsyslog.d/

echo "restart rsyslog service"
service rsyslog restart

echo "get IP addresses of reverse proxy"
${nova_list_command} | grep reverse_proxy > ${scale_dir}/servers.rp.aux
revproxy_ip_int=`awk -F'[=,|]' '{print $10}' ${scale_dir}/servers.rp.aux`
revproxy_ip_ext=`awk -F'[=,|]' '{print $8}' ${scale_dir}/servers.rp.aux`

echo "get IP of control server"
control_ip=`ifconfig eth0 | awk -F ' *|:' '/inet addr/{print $4}'`

echo "put reverse proxy IP in etc hosts on control server"
echo "${revproxy_ip_int}reverse_proxy" >> /etc/hosts

echo "put control IP in etc hosts on reverse proxy server"
ssh -i /home/ubuntu/.ssh/id_rsa -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no ubuntu@${revproxy_ip_int} "echo ${control_ip} control_srv | sudo tee -a /etc/hosts > /dev/null"

echo "replace IPs in haproxy temp cfg"
sed -i "s/control_ip/$control_ip/g" ${scale_dir}/haproxy.cfg.tmp
sed -i "s/revproxy_ip_ext/${revproxy_ip_ext}/g" ${scale_dir}/haproxy.cfg.tmp

echo "create web stack"
${heat_create_command}

echo "wait 6 minutes"
sleep 360

echo "source functions file"
. ${git_dir}/sh/_functions

echo "deploy web site"
echo "" > ${scale_dir}/servers.list
_refresh_web_ips init

echo "set owner ubuntu"
chown -R ubuntu:ubuntu ${scale_dir}

echo "set permissions"
chmod -R 755 ${scale_dir}

echo "create sqlite db"
curl -s http://localhost:80/_install.php
echo ""

echo "remove install page"
rm -f /var/www/html/_install.php

echo "set cron job"
echo "* * * * *	ubuntu	bash ${scale_dir}/sh/probe.sh >/dev/null 2>&1" > /etc/cron.d/scale
