#!/usr/bin/env bash

echo "source parameters, variables and functions"
. /opt/scale/sh/_variables
. ${scale_sh}/_parameters
. ${scale_sh}/_functions

hash_cpu=`awk -F\; '{print $2}' ${log_cpu} | tail -${EVAL_PERIOD}`
hash_time=`awk -F\; '{print $2}' ${log_time} | tail -${EVAL_PERIOD}`
already_updated=0

echo "apply the evaluation method"

case "${EVAL_METHOD}" in
    0)
        _eval_average_cpu;;
    1)
        _eval_average_cpu
        if [ ${already_updated} -eq 0 ]
            then
                _eval_average_time
        fi;;
esac