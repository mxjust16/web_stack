#!/usr/bin/env bash

echo "source parameters, variables and functions"
. /opt/scale/sh/_variables
. ${scale_sh}/_parameters
. ${scale_sh}/_functions

timestamp_log=`date +%Y%m%d-%H%M`
timestamp_haproxy=`date -d "1 minute ago" +%d/%b/%Y:%H:%M`

echo "calculate average response time"
time_avg=`grep "${timestamp_haproxy}" /var/log/haproxy.log | awk '{print $5}' | awk -F\/ '{sum+=$5} END {if (NR>0) print int(sum/NR)}'`
time_avg="${time_avg:-0}"

echo "calculate average CPU usage"
servers=`grep web_server /etc/hosts | awk '{print $1}'`
cpu_t=0
iter=0
for i in ${servers}; do
	cpu=`ssh -o ConnectTimeout=15 -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no $i "sar -u 1 3 | tail -1 | rev | cut -d\  -f1 | rev"` || cpu=0
	if [ "$cpu" != "0" ]
	    then
	        cpu_t=`echo "${cpu_t}+${cpu}"`
	        iter=$((iter+1))
	fi
done
cpu_avg=`echo "scale=1; 100-($cpu_t)/$iter" | bc`

echo "write values in log files"
echo "${timestamp_log};${cpu_avg}" >> ${log_cpu}
echo "${timestamp_log};${time_avg}" >> ${log_time}

case "${SCALE_MODE}" in
    0)
        exit 0;;
    1)
        if [ ! -f ${do_not_update_flag} ]
            then
                update_possible=1
                _check_if_update_possible
                if [ "${update_possible}" -eq 1 ]
                    then
                        bash ${scale_sh}/evaluation.sh
                    else
                        _write_to_log "not enough time since last update"
                fi
            else
                _write_to_log "update in progress"
        fi;;
esac
