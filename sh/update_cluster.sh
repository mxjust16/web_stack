#!/usr/bin/env bash

echo "source parameters, variables and functions"
. /opt/scale/sh/_variables
. ${scale_sh}/_parameters
. ${scale_sh}/_functions

echo "get cluster size"
cluster_size=`heat stack-show web | grep cluster_size | sed 's/|//g' | sed 's/ \+//g' | sed 's/"//g' | cut -d: -f2`
update_command="heat stack-update web -f ${scale_dir}/heat/web_cluster_deploy.yaml -e ${scale_dir}/heat/env_web_cluster.yaml -P cluster_size="
case $1 in
    down)
        if [ ${cluster_size} -gt ${CLUSTER_SIZE_MIN} ]
            then
                touch ${do_not_update_flag}
                cluster_size=$((cluster_size-1))
                _write_to_log " downsize cluster to ${cluster_size}"
                ${update_command}${cluster_size}
                sleep 60
                _refresh_web_ips
                rm ${do_not_update_flag}
            else
                _write_to_log " cluster already at minimum size"
        fi;;
    up)
        if [ ${cluster_size} -lt ${CLUSTER_SIZE_MAX} ]
            then
                touch ${do_not_update_flag}
                cluster_size=$((cluster_size+1))
                _write_to_log " upsize cluster to ${cluster_size}"
                ${update_command}${cluster_size}
                sleep 360
                _refresh_web_ips $1
                rm ${do_not_update_flag}
            else
                _write_to_log " cluster already at maximum size"
        fi;;
esac