#!/usr/bin/env bash

echo "source parameters, variables and functions"
. /opt/scale/sh/_variables
. ${scale_sh}/_parameters
. ${scale_sh}/_functions

if [ ! -f ${do_not_update_flag} ]
    then
        ${scale_sh}/update_cluster.sh down
    else
        _write_to_log "update already in progress"
fi
